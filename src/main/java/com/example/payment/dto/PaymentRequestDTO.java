package com.example.payment.dto;

import java.math.BigDecimal;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
 
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
 
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PaymentRequestDTO {
	@NotBlank(message = "Order ID is required")
	private String orderId;
	@NotNull(message = "Amount is required")
	private BigDecimal amount;
 
}